const express = require('express');
const bodyParser = require('body-parser');
const client = require('./koneksi');
const fs = require('fs');
const app = express();
const port = 3000;

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

app.listen(port, () => {
  console.log(`Server berjalan di port ${port}`);
});

client.connect();

app.get('/provinsi', (req, res) => {
  client.query(`Select * from provinsi`, (err, result) => {
    if (!err) {
      res.send(result.rows);
    }
  });
  client.end;
});

app.get('/provinsi/:id_provinsi', (req, res) => {
  client.query(`Select * from provinsi where id_provinsi=${req.params.id_provinsi}`, (err, result) => {
    if (!err) {
      res.send(result.rows);
    }
  });
  client.end;
});

app.post('/provinsi', (req, res) => {
  const prov = req.body;
  let insertQuery = `insert into provinsi(id_provinsi, nama) 
                     values(${prov.id_provinsi}, '${prov.nama}')`;

  client.query(insertQuery, (err, result) => {
    if (!err) {
      res.send('Sukses menambah data');
    } else {
      console.log(err.message);
    }
  });
  client.end;
});

app.put('/provinsi/:id_provinsi', (req, res) => {
  let prov = req.body;
  let updateQuery = `update provinsi
                     set nama = '${prov.nama}'
                     where id_provinsi = ${prov.id_provinsi}`;

  client.query(updateQuery, (err, result) => {
    if (!err) {
      res.send('Update successful');
    } else {
      console.log(err.message);
    }
  });
  client.end;
});

app.delete('/provinsi/:id_provinsi', (req, res) => {
  let insertQuery = `delete from provinsi where id_provinsi=${req.params.id_provinsi}`;

  client.query(insertQuery, (err, result) => {
    if (!err) {
      res.send('Delete successful');
    } else {
      console.log(err.message);
    }
  });
  client.end;
});
