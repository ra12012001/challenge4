const { Pool } = require('pg');
const kotaData = require('../data/kota.json');
const pool = new Pool({
  user: 'postgres',
  host: 'localhost',
  database: 'dbntb',
  password: 'rudi',
  port: 5432,
});

let query = `INSERT INTO
kota (id_kota, nama, id_provinsi)
VALUES`;

kotaData.forEach((eachData, i) => {
  if (i !== kotaData.length - 1) {
    query += `('${eachData.id_kota}', '${eachData.nama}','${eachData.id_provinsi}'),`;
  } else {
    query += `('${eachData.id_kota}', '${eachData.nama}','${eachData.id_provinsi}');`;
  }
});

pool
  .query(query)
  .then(() => {
    console.log('Data seeding success');
    pool.end();
  })
  .catch((error) => {
    console.error(error);
  });
