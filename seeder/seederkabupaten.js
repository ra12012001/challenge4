const { Pool } = require('pg');
const kabData = require('../../data/kabupaten.json');
const pool = new Pool({
  user: 'postgres',
  host: 'localhost',
  database: 'dbntb',
  password: 'rudi',
  port: 5432,
});

let query = `INSERT INTO
kabupaten (id_kabupaten, nama, id_kota)
VALUES`;

kabData.forEach((eachData, i) => {
  if (i !== kabData.length - 1) {
    query += `('${eachData.id_kabupaten}', '${eachData.nama}','${eachData.id_kota}'),`;
  } else {
    query += `('${eachData.id_kabupaten}', '${eachData.nama}','${eachData.id_kota}');`;
  }
});

pool
  .query(query)
  .then(() => {
    console.log('Data seeding success');
    pool.end();
  })
  .catch((error) => {
    console.error(error);
  });
