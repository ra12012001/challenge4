const { Pool } = require('pg');
const provData = require('../../data/prov.json');
const pool = new Pool({
  user: 'postgres',
  host: 'localhost',
  database: 'dbntb',
  password: 'rudi',
  port: 5432,
});
// console.log(provData);
let query = `INSERT INTO
provinsi (id_provinsi, nama)
VALUES`;

provData.forEach((eachData, i) => {
  if (i !== provData.length - 1) {
    query += `('${eachData.id_provinsi}', '${eachData.nama}'),`;
  } else {
    query += `('${eachData.id_provinsi}', '${eachData.nama}');`;
  }
});

pool
  .query(query)
  .then(() => {
    console.log('Data seeding success');
    pool.end();
  })
  .catch((error) => {
    console.error(error);
  });
