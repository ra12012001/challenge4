const { Client } = require('pg');

const client = new Client({
  user: 'postgres',
  host: 'localhost',
  database: 'dbntb',
  password: 'rudi',
  port: 5432,
});

module.exports = client;
